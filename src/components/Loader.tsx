import React from 'react'

import { InfinitySpin } from 'react-loader-spinner'
type Props = {}

const Loader = (props: Props) => {
  return (
    <div className='absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 transform'>
      <InfinitySpin width='200' color='white' />
    </div>
  )
}

export default Loader
