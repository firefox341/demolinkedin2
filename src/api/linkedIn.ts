import axios from 'axios'
interface codeProps {
  code: String
}
// export const getAccessToken = async (payloadCode: any) => {
//   const response = await fetch(
//     'https://www.linkedin.com/oauth/v2/accessToken',
//     {
//       method: 'POST',

//       body: JSON.stringify({
//         grant_type: 'authorization_code',
//         code: payloadCode,
//         redirect_uri: 'http://localhost:3000',
//         client_id: '86e59crv4uuo0f',
//         client_secret: 'gt3SYGO7Zw8jNXLP'
//       })
//     }
//   )
//   //   if (!response.ok) {
//   //     throw new Error(`HTTP error! status: ${response.status}`)
//   //   }

//   const result = await response.json()
//   return result?.access_token
// }
export const getAccessToken = async (payloadCode: any) => {
  const body = new URLSearchParams()
  body.append('grant_type', 'authorization_code')
  body.append('code', payloadCode)
  body.append('redirect_uri', 'https://demolinkedin2.vercel.app/')
  body.append('client_id', '86e59crv4uuo0f')
  body.append('client_secret', 'gt3SYGO7Zw8jNXLP')

  const response = await fetch(
    'https://www.linkedin.com/oauth/v2/accessToken',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: body
    }
  )

  //   if (!response.ok) {
  //     throw new Error(`HTTP error! status: ${response.status}`)
  //   }

  const result = await response.json()
  return result
  //   return response.json()
}

// export const getUserDetails = async (token: string) => {
//   const res = await fetch('https://api.linkedin.com/v2/me', {
//     method: 'GET',
//     headers: {
//       Authorization: 'Bearer ' + token
//     },
//     mode: 'no-cors'
//   })
//   console.log(res)
// }
export const getUserDetails = async (token: string) => {
  try {
    const response = await fetch(
      `https://api.linkedin.com/v2/me?oauth2_access_token=${token}`,
      {
        method: 'GET'
      }
    )
    const res = response.json()
    return res
  } catch (error) {
    console.error(error)
  }
}

export const getUserEmail = async (token: string) => {
  try {
    const response = await fetch(
      `https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token=${token}`,
      {
        method: 'GET'
      }
    )
    const res = response.json()
    return res
  } catch (error) {
    console.error(error)
  }
}

export const getUserConnections = async (token: string) => {
  try {
    const res = await fetch(
      `https://api.linkedin.com/v2/connections?q=viewer&start=0&count=50)&oauth2_access_token=${token}`,
      {
        method: 'GET'
      }
    )
    return res.json()
  } catch (err) {
    console.error(err)
  }
}
