import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import { Inter } from '@next/font/google'
import { useRouter } from 'next/router'
import Loader from '@/components/Loader'
import {
  getUserDetails,
  getAccessToken,
  getUserEmail,
  getUserConnections
} from '@/api/linkedIn'
const inter = Inter({ subsets: ['latin'] })

export default function Home () {
  interface User {
    localizedLastName: string
    firstName: {
      localized: { en_US: string }
      preferredLocale: { country: string; language: string }
    }
    lastName: {
      localized: { en_US: string }
      preferredLocale: { country: string; language: string }
    }
    id: string
    localizedFirstName: string
  }
  const mock = {
    localizedLastName: 'Nambiar',
    firstName: {
      localized: { en_US: 'subnumb' },
      preferredLocale: { country: 'US', language: 'en' }
    },
    lastName: {
      localized: { en_US: 'Nambiar' },
      preferredLocale: { country: 'US', language: 'en' }
    },
    id: 'CUb30XQCU-',
    localizedFirstName: 'subnumb'
  }
  const router = useRouter()
  const signCode = router?.query?.code
  const [accessToken, setAccessToken] = useState(null)
  const [user, setUser] = useState<User | null>(null)
  const [userEmail, setUserEmail] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
    const fetchAccessToken = async () => {
      setIsLoading(true)
      const accessToken = await getAccessToken(signCode)
      setAccessToken(accessToken?.access_token)
      setIsLoading(false)
    }
    if (signCode) {
      fetchAccessToken()
    }
  }, [signCode])
  useEffect(() => {
    const fetchUserDetails = async (token: string) => {
      setIsLoading(true)
      const userDetails = await getUserDetails(token)
      userDetails && setUser(userDetails)
      setIsLoading(false)
    }
    if (accessToken) {
      fetchUserDetails(accessToken)
      getUserConnections(accessToken)
    }
  }, [accessToken])
  useEffect(() => {
    const fetchUserEmail = async (token: string) => {
      setIsLoading(true)
      const userEmailDetails = await getUserEmail(token)
      if (userEmailDetails) {
        setUserEmail(userEmailDetails?.elements[0]?.['handle~']?.emailAddress)
      }
      setIsLoading(false)
    }
    if (user && accessToken) {
      fetchUserEmail(accessToken)
    }
  }, [user])
  return (
    <div className=' flex flex-col justify-center p-10 align-middle'>
      <div className='flex justify-center rounded-3xl border border-sky-400 p-10'>
        LinkedIn Login
      </div>
      {isLoading && <Loader />}
      {user ? (
        <div className='p-10 flex justify-evenly'>
          <div className='flex flex-col'>
            <span className='text-5xl m-10 capitalize text-slate-50'>
              {' '}
              Hi,
              {user?.localizedFirstName + ' ' + user?.localizedLastName}
            </span>
            <span className='text-2xl text-sky-400 m-10'>
              Email: {userEmail}
            </span>
          </div>
          <span
            className='bg-slate-50 rounded-2xl p-3 h-12 text-slate-900 cursor-pointer'
            onClick={() => {
              setUser(null)
              router.replace('/')
            }}
          >
            Logout
          </span>
        </div>
      ) : (
        <div className='m-5 flex h-48 w-64 flex-col  justify-between rounded-md border border-sky-300 p-3 align-middle'>
          <span className='text-lg'>Register and join the community</span>
          <span className='text-xs text-gray-400'>
            Post your socical feed.See whose going.Invite your network
          </span>
          <a href='https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86e59crv4uuo0f&redirect_uri=http://localhost:3000&scope=r_liteprofile%20r_emailaddress%20w_member_social%20r_1st_connections_size%20r_organization_admin%20rw_ads%20r_ads%20rw_organization_admin%20r_organization_social'>
            <div className='flex rounded-md bg-sky-700 align-middle'>
              <img
                src={
                  'https://www.drupal.org/files/project-images/linkedin_circle_logo.png'
                }
                alt='logo'
                style={{
                  width: '40px',
                  height: '40px',
                  cursor: 'pointer',
                  alignItems: 'center',
                  margin: '5px',
                  backgroundColor: 'white',
                  borderRadius: '50%'
                }}
              />
              <span className='p-3 text-slate-200'>Login with LinkedIn</span>
            </div>
          </a>
        </div>
      )}
    </div>
  )
}
